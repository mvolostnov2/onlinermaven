package by.onliner.webapp.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class RadiocontrolAirPage {
    private static final Logger log = Logger.getLogger(RadiocontrolAirPage.class);

    private WebDriver driver;
    JavascriptExecutor js = (JavascriptExecutor) driver;

    public RadiocontrolAirPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

//    private  final WebElement compareButton = driver.findElement(By.xpath("//div[@class='compare-button-container']//span[contains(text(),'товара')]"));
    private final By compareButton = By.xpath("//div[@class='compare-button-container']//span[contains(text(),'товара')]");


    public WebElement leftMenuItem(String leftMenuItem) {
        return driver.findElement(By.xpath(String.format("//div[@id='schema-filter']//span[text()='%s']", leftMenuItem)));
    }

    /*
    public WebElement sortingMenu() {
        return driver.findElement(By.xpath("//div[@class='js-schema-results schema-grid__center-column']//a[@class='schema-order__link']"));
    }

     */

    private  final By sortingMenu = By.xpath("//div[@class='js-schema-results schema-grid__center-column']//a[@class='schema-order__link']");

    public WebElement sortingMenuItem(String sortingMenuItem) {
        return driver.findElement(By.xpath(String.format("//div[@class='schema-order__popover']//span[text()='%s']"
                , sortingMenuItem)));
    }

    public WebElement schemaProductPriceGroup() {
        return driver.findElement(By.xpath("//div[@class='schema-product__price-group']"));
    }

    public WebElement compareButtonWithProducts(int numberOfProducts) {
        return driver.findElement(By.xpath(String.format("//div[@class='compare-button compare-button_visible']//span[contains(text(),'%d')]"
                ,numberOfProducts)));
    }
/*
    public WebElement compareButton() {
        return driver.findElement(By.xpath("//div[@class='compare-button-container']//span[contains(text(),'товара')]"));
    }
*/
    public WebElement numberOfSearchResults(int numberOfSearchResults) {
        return driver.findElement(By.xpath(String.format("//div[@class='schema-filter-button__state schema-filter-button__state_initial schema-filter-button__state_disabled schema-filter-button__state_control']//span[contains(text(),'Найдено %d')]"
                ,numberOfSearchResults )));

    }

    public RadiocontrolAirPage scrollToLeftMenuElement(String leftMenuElement) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(String.format("//div[@id='schema-filter']//span[text()='%s']", leftMenuElement))));
        log.info("Scroll to Left Menu Element: " + leftMenuElement);
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage scrollToPageHeader() throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
                driver.findElement(By.xpath("//h1[@class='schema-header__title']")));
        log.info("Scroll to Page Header" );
        return this;
    }

    public RadiocontrolAirPage selectLeftMenuItem(String leftMenuItem)  {
        new WebDriverWait(driver, 10)
                .until(elementToBeClickable(leftMenuItem(leftMenuItem)));
        leftMenuItem(leftMenuItem).click();
        log.info("Select Left Menu Item: " + leftMenuItem);
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage selectSortingBy(String sortingMenuItem) throws InterruptedException {

        new WebDriverWait(driver, 10).until(elementToBeClickable(sortingMenu));
        driver.findElement(sortingMenu).click();
        log.info("Open Sorting menu");
        new WebDriverWait(driver, 10).ignoring(ElementClickInterceptedException.class)
                .until(elementToBeClickable(By.xpath("//div[@class='js-schema-results schema-grid__center-column']//a[@class='schema-order__link']")));

        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", sortingMenuItem(sortingMenuItem));

//        sortingMenuItem(sortingMenuItem).click();
        log.info("Select Sorting by: " + sortingMenuItem);

        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage setMinimalRange(String minimalRage)  {
        driver.findElement(By.xpath("//div[@id='schema-filter']//input[@placeholder=5]")).sendKeys(minimalRage);
        log.info("Set Minimal Range: " + minimalRage);
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage openAdditionalParameters()  {
        driver.findElement(By.xpath("//div[@id='schema-filter']//a[@data-bind='click: $root.toggleAdditionalParameters.bind($root)']")).click();
        log.info("Open Additional Parameters");
        return new RadiocontrolAirPage(driver);
    }

    public RadiocontrolAirPage selectProductByIndex(int index) throws InterruptedException {
        new WebDriverWait(driver, 10).ignoring(ElementClickInterceptedException.class)
                .until(visibilityOfElementLocated(By.xpath("//div[@id='schema-products']//span[@class='i-checkbox__faux']")));
        List<WebElement> selectedProducts = driver.findElements(By.xpath("//div[@id='schema-products']//span[@class='i-checkbox__faux']"));

        Actions actions = new Actions(driver);
        actions.moveToElement(selectedProducts.get(index)).click().build().perform();

        log.info("Select product № " + index);
        return this;
    }

    public ComparisonPage openComparison()  {
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(compareButton));
  //      compareButton.click();
        driver.findElement(compareButton).click();
        log.info("Open Comparison");
        return new ComparisonPage(driver);
    }


    public RadiocontrolAirPage checkThatPageHeaderContains(String headerName) {
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//h1[@class='schema-header__title']")));

        String pageHeader = driver.findElement(By.xpath("//h1[@class='schema-header__title']")).getText();
        log.info("Checking Page Header");
        if (pageHeader.contains(headerName)) {
            log.info("Page header name is correct: " + pageHeader);
        } else {
            log.info("Warning! Page header name is incorrect: " + pageHeader + " instead of " + headerName);
        }
//        assertThat(pageHeader).as("Incorrect Page header", pageHeader).contains(headerName);
        return this;
    }

    public RadiocontrolAirPage verifyThatNumberOfSearchResultsEqualsTo(int numberOfSearchResults) {
        log.info("Verification that Number of Search Results is correct");

       new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath(String.format("//div[@class='schema-filter-button__state schema-filter-button__state_initial schema-filter-button__state_disabled schema-filter-button__state_control']//span[contains(text(),'Найдено %d')]"
                        , numberOfSearchResults ))));


        log.info("numberOfSearchResults: " + numberOfSearchResults);
        assertThat(numberOfSearchResults(numberOfSearchResults).isDisplayed()).as("Найдено: " + numberOfSearchResults).isTrue();
        return this;
    }



    public RadiocontrolAirPage verifyThatSortingIsCorrect() throws InterruptedException {
        log.info("Verify that Sorting is correct");
/*
        new WebDriverWait(driver, 10).ignoring(StaleElementReferenceException.class)
                .until(visibilityOfElementLocated(By.xpath("//span[text()='42,00 р.']")));

 */
/*
        new WebDriverWait(driver, 10)
                .until(visibilityOfElementLocated(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'р.')]")));

 */
 //       driver.manage().timeouts().pageLoadTimeout(2000,TimeUnit.MILLISECONDS);
 //       driver.manage().timeouts().implicitlyWait(2000,TimeUnit.MILLISECONDS);
//        driver.manage().timeouts().wait(2000);

Thread.sleep(1000);
 //       new WebDriverWait(driver, 10)
 //               .until(visibilityOfElementLocated(By.xpath("//div[@id='schema-products']//span[contains(text(),'квадрокоптер')]")));


        List<Double> priceToDouble = new ArrayList<Double>();
        List<WebElement> price = driver.findElements(By.xpath("//div[@class='schema-product__part schema-product__part_2']//span[contains(text(),'р.')]"));

        for (WebElement element : price) {
            priceToDouble.add(Double.parseDouble(element.getText().replaceAll("[^,0-9]+", "").replaceAll(",", ".")));
        }
/*
        boolean isSorted = Ordering.natural().isOrdered(priceToDouble);
        System.out.println(isSorted);

 */
/*
        boolean isSorted = Comparators.isInOrder(priceToDouble, Comparator.<Double> naturalOrder());
        System.out.println("Whyyyyy??????? " + isSorted);

 */


/*
        String verifySortingOrder = (priceToDouble.get(1) >= priceToDouble.get(0)) ? "Sorting is correct!" : "Sorting is incorrect!";
        System.out.println(verifySortingOrder);

 */

        assertThat(priceToDouble.get(1) >= priceToDouble.get(0)).as("Sorting is incorrect! "  + priceToDouble.get(1) + " is not >= " + priceToDouble.get(0)).isTrue();
        log.info("Sorting is Correct: " + priceToDouble.get(1) + " > " + priceToDouble.get(0));

        return this;
    }

/*

    public static boolean sortingIsCorrect (List<Double> priceToDouble) {

        log.info("!!! sortingIsCorrect !!!");

        return Comparators.isInOrder(priceToDouble, Comparator.<Double> naturalOrder());

    }
*/


        public RadiocontrolAirPage verifyThatCompareButtonContainsNumberOfSelectedProducts(int numberOfProducts) throws InterruptedException {
            log.info("Verify That Compare Button contains " + numberOfProducts + " products");

//Thread.sleep(1000);
/*
            new WebDriverWait(driver, 10)
                    .until(elementToBeClickable(compareButtonWithProducts(numberOfProducts)));

 */

            new WebDriverWait(driver, 10)
                    .until(elementToBeClickable(By.xpath("//div[@class='compare-button compare-button_visible']//span[contains(text(),'4')]")));

   //     Assert.assertTrue(compareButtonWithProducts(numberOfProducts).isDisplayed(), "Cant find Compare Button with " + numberOfProducts + " products");
        assertThat(compareButtonWithProducts(numberOfProducts).isDisplayed()).as("Cant find Compare Button with " + numberOfProducts + " products").isTrue();
            log.info("Passed");
            return this;
        }

}
