package by.onliner.test;

import by.onliner.webapp.WebApplication;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.time.LocalDateTime;


@Log4j
public class BaseTest {

    public static WebDriver driver;
    private static final String HOMEPAGE_URL = "https://onliner.by/";
    protected static WebApplication app = new WebApplication();
    private static final String SCREENSHOTS_PATH = "D:\\Automation\\GitHub\\onlinertest\\screenshots\\";


    @BeforeSuite(alwaysRun = true)
    public void browserSetup() {

        driver = initDriver(BrowserType.CHROME);
        log.info("Open browser " + driver);

        driver.manage().window().maximize();
        log.info("Maximize browser's window");

    }

    @BeforeMethod
    public void openPage() {
        driver.get(HOMEPAGE_URL);
        log.info("Open https://onliner.by/");

    }

    @AfterSuite(alwaysRun = true)
    public void browserTearDown() {
        log.info("Close browser" + driver);
        driver.quit();
        driver=null;

    }


    @AfterMethod //AfterMethod annotation - This method executes after every test execution
    public void screenShot(ITestResult result){
        //using ITestResult.FAILURE is equals to result.getStatus then it enter into if condition
        if(ITestResult.FAILURE==result.getStatus()){
            try{
                // To create reference of TakesScreenshot
                TakesScreenshot screenshot=(TakesScreenshot)driver;
                // Call method to capture screenshot
                File src=screenshot.getScreenshotAs(OutputType.FILE);
                // Copy files to specific location
                // result.getName() will return name of test case so that screenshot name will be same as test case name
                FileUtils.copyFile(src, new File(SCREENSHOTS_PATH + LocalDateTime.now().toString().replaceAll(":", "-") + result.getName() + ".png"));
                System.out.println("Successfully captured a screenshot");
            }catch (Exception e){
                System.out.println("Exception while taking screenshot "+e.getMessage());
            }
        }
        driver.quit();
    }


    private WebDriver initDriver(String browserType) {
        if (browserType.equals(BrowserType.CHROME)) {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver();
        }
        if (browserType.equals(BrowserType.FIREFOX)) {
            WebDriverManager.firefoxdriver().setup();
            return new FirefoxDriver();
        }
        if (browserType.equals(BrowserType.EDGE)) {
            WebDriverManager.edgedriver().setup();
            return new EdgeDriver();
        }
        return null;
    }

}
